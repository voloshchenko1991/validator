package com.example.demo;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class FormExtendedValidatior implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return FormExtended.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        FormExtended formExtended = (FormExtended)o;
        if(formExtended.getFirstName().isEmpty()){
            errors.rejectValue("firstName","601","Name has to be filled!");
        }
    }
}
