package com.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class MyRestController {

    @Autowired
    FormExtendedValidatior formExtendedValidatior;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(formExtendedValidatior);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void acceptFormSimple(@RequestBody @Valid FormSimple formSimple){}

    @RequestMapping(value = "/extended", method = RequestMethod.POST)
    public void acceptFormExtended(@RequestBody @Valid FormExtended formExtended, BindingResult bindingResult){}

}
