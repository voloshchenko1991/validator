package com.example.demo;

import lombok.Data;

@Data
public class FormExtended {

   private String firstName;

   private String lastName;

   private Address address;

}
