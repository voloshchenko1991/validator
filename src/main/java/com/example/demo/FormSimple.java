package com.example.demo;

import lombok.Data;
import org.hibernate.validator.constraints.Email;


import javax.validation.constraints.NotNull;

@Data
public class FormSimple {

    @NotNull
    @Email
    private String email;

    @NotNull
    private String password;

}
